const express= require('express'); // con esta variable vamos a tener disponible este framework
const app= express(); // esto lo inicia

const authController = require('./controllers/AuthController');
const io = require('./io');

app.use(express.json());

const port = process.env.PORT || 3000; // aquí se da preferencia a la variable de entorno (por convención se ponen en MAY) y si no, se coge el puerto 3000

app.listen(port);
console.log("API serverLogin Escuchando en puerto port BIP BIP: "+port);

const authController = require('./controllers/AuthController');

app.post('/apitechu/v1/login',authController.loginV1);
app.post('/apitechu/v1/logout/:id',authController.logoutV1);
