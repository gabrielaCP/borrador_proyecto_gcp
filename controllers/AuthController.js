//AuthController

const requestJson = require('request-json');
const crypt = require('../crypt');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechugcp9ed/collections/";
const mlabApiKey = "apiKey=" + process.env.MLAB_API_KEY;//"apiKey=RNNUfiWJrCqaCCoV8rB9ArGP1JEQv4Mv"; //Ojo, para la petición http se tiene que devolver el apiKey con el valor



function loginV1(req, res){
    console.log("POST  /apitechu/v1/login");
    //console.log(req);
      var io = require('../io.js');
    var users = require('../usersPass.json');
    var id;
    var login = false;
    for (var i = 0; i < users.length; i++) {
      // console.log("comparando " + users[i].email + " y " +  req.body.email);
      // console.log("comparando " + users[i].password + " y " +  req.body.password);
       if ((users[i].email == req.body.email) && (users[i].password == req.body.password)) {
         console.log("Todo coincide");
         users[i].logged= true;
          console.log( users[i].logged);
         io.writeUserDataToFile(users);
         id= users[i].id;
         login = true;
         break;
     }

    }
    if(login){
      res.send({"Login correcto: id: " : id});
  }else{
    res.send({"Login incorrecto" : id});
  }
}

function loginProfeV1(req, res){
    console.log("POST  /apitechu/v1/login");
    //console.log(req);
    var io = require('../io.js');

    var id;
    var login = false;
    for (user of users) {
      // console.log("comparando " + users[i].email + " y " +  req.body.email);
      // console.log("comparando " + users[i].password + " y " +  req.body.password);
       if ((users.email == req.body.email) && (users.password == req.body.password)) {
         console.log("Todo coincide");
         users.logged= true;
         var loggedUserId = user.id;
        console.log( users.logged);
         io.writeUserDataToFile(users);
         id= users.id;
         login = users.logged;
         break;
     }

    }
    var msg = login ? "Login correcto" : "Login incorrecto";
    var response = {
    "mensaje": msg,
    "idUsuario": loggedUserId
    };

    res.send(response);
}

function loginProfeV2(req, res){

    console.log("POST  /apitechu/v2/login");

    var httpClient = requestJson.createClient(baseMlabURL);
    var mailUser = req.body.email;
    var passUser= req.body.password;

    console.log("mailUser: "+mailUser+ " passUser: "+passUser);

    var queryConsulta = 'q={"email": "'+mailUser+'"}';
    console.log (queryConsulta);

    httpClient.get("user/?"+queryConsulta+"&"+mlabApiKey,
       function(err, resMLab, body){
         if (err){
           var response = {
             "msg" : "Error obteniendo usuario"
           }
           res.status(401).send(response);
         }else{
            console.log("Ha ido bien");
           if(body.length > 0){
             var response = body[0];
             passwordFromDBHashed = response.password;
             var idUsuario = response.id;
             console.log("passwordFromDBHashed" + passwordFromDBHashed);
             if(passwordFromDBHashed == passUser){
               console.log("Coinciden");
               console.log("idUsuario");
               //Hacemos la actualización

               var putBody = '{"$set": {"logged": true}}';
               console.log(putBody);
               httpClient.put("user?"
                   +queryConsulta
                   +"&"
                   +mlabApiKey, JSON.parse(putBody),
                   function(err, resMLab, body){
                       if (err){
                         var response = {
                           "msg" : "Error modificando usuario"
                         }
                         console.log(resMLab);
                         res.send(response);
                       }else{
                         console.log("Usuario logado con éxito : id "+idUsuario);
                         console.log(body);
                        res.status(200);
                        var response = {"msg" : "usuario logado", "id" : idUsuario};
                        res.send(response);
                      }
                   }
                 )
             }

           }else{
             var response= {
               "msg" : "Usuario no encontrado"
             }
             res.send(response);
           }
         }

       }
     );
}

function logoutV2(req, res){

    console.log("POST  /apitechu/v1/logoutV2/:id");
    console.log(req.params.id);
    var idUser = req.params.id;
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("idUser: "+idUser);
    var query = 'q={"id": '+idUser+'}';

    httpClient.get("user/?"+query+"&"+mlabApiKey,
       function(err, resMLab, body){
         if (err){
           var response = {
             "msg" : "Error obteniendo usuario"
           }
           res.send(response);
         }else{
            console.log("Ha ido bien");
            var response = body[0];
            var logged = response.logged;
            var idUsuario= response.id;
            console.log("logado: " + logged);

            var putBody = '{"$unset": {"logged": ""}}';
            console.log(putBody);
            //Hacemos la query con el ID que recogemos de la consulta por si acaso
            var queryPUT = 'q={"id": '+idUsuario+'}';
            httpClient.put("user?"
                +queryPUT
                +"&"
                +mlabApiKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT){
                    if (errPUT){
                        var response = {
                          "msg" : "Error modificando usuario"
                        }
                        console.log(errPUT);
                        res.send(response);
                    }else{
                        console.log("Usuario deslogado con éxito : id "+idUsuario);
                        console.log(bodyPUT);
                       res.status(200);
                       var response = {"msg" : "usuario deslogado", "id" : idUsuario};
                       res.send(response);
                   }
                }
              ) //Cierre put
          }//cierre else
        }
        )// cierre get
      }

function logoutV1(req, res){
    console.log("POST  /apitechu/v1/logoutV1/:id");
    console.log(req.params.id);

    var io = require('../io.js');
    var users = require('../usersPass.json');
    var logout= false;
    for (var i = 0; i < users.length; i++) {
       console.log("comparando " + users[i].id + " y " +  req.params.id);
      // console.log("comparando " + users[i].password + " y " +  req.body.password);
       if ((users[i].id == req.params.id) && (users[i].logged == true)) {
         console.log("Todo coincide");
         delete users[i].logged;
         io.writeUserDataToFile(users);
         logout= true;
         break;
     }
    }

    if(logout){
      res.send({"Logout correcto: id: " : req.params.id});
    }else{
      res.send({"Logout incorrecto": req.params.id});
    }
}

const io = require('../io');

function loginProfeV1(req, res) {
 console.log("POST /apitechu/v1/login");

 var users = require('../usersPass.json');
 for (user of users) {
   if (user.email == req.body.email
       && user.password == req.body.password) {
     console.log("Email found, password ok");
     var loggedUserId = user.id;
     user.logged = true;
     console.log("Logged in user with id " + user.id);
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedUserId ?
   "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedUserId
 };

 res.send(response);
}




function loginProfeV1(req, res) {
 console.log("POST /apitechu/v1/login");

 var users = require('../usersPass.json');
 for (user of users) {
   if (user.email == req.body.email
       && user.password == req.body.password) {
     console.log("Email found, password ok");
     var loggedUserId = user.id;
     user.logged = true;
     console.log("Logged in user with id " + user.id);
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedUserId ?
   "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedUserId
 };

 res.send(response);
}

function logoutProfeV1(req, res) {
 console.log("POST /apitechu/v1/logout");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.id == req.params.id && user.logged === true) {
     console.log("User found, logging out");
     delete user.logged
     console.log("Logged out user with id " + user.id);
     var loggedoutUserId = user.id;
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedoutUserId ?
   "Logout correcto" : "Logout incorrecto";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedoutUserId
 };

 res.send(response);
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginProfeV2 = loginProfeV2;
module.exports.logoutV2 = logoutV2;
