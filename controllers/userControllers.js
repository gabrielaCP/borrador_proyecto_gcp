const requestJson = require('request-json');
const crypt = require('../crypt');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechugcp9ed/collections/";
const mlabApiKey = "apiKey=" + process.env.MLAB_API_KEY;//"apiKey=RNNUfiWJrCqaCCoV8rB9ArGP1JEQv4Mv"; //Ojo, para la petición http se tiene que devolver el apiKey con el valor

function getUsersV2(req,res) {
   console.log("GET /apitechu/v2/users");

   var httpClient = requestJson.createClient(baseMlabURL);
   console.log("Client created");

   httpClient.get("user?"+mlabApiKey,
      function(err, resMLab, body){
        // err - devolverá el 400 o el 500
        // resMLab es el que tiene toda la información sobre los posibles errores de la obtención de usuarios.
        var response = !err ? body : {
            "msg" : "Error obteniendo usuarios"
        }
        res.send(response);
      }
    );
}

function getUserByIdV2(req,res) {
   console.log("GET /apitechu/v2/users/:id");
   var httpClient = requestJson.createClient(baseMlabURL);
   console.log("Client created");
   var idUser = req.params.id;
   console.log("idUser: "+idUser);
   var query = 'q={"id": '+idUser+'}';
   console.log (query);
   /* Refactorizamos
   httpClient.get("user/?"+query+"&"+mlabApiKey,
      function(err, resMLab, body){
        var response = !err ? body : {
            "msg" : "Error obteniendo usuarios"
        }
        res.send(response);
      }
    );
    */
    httpClient.get("user/?"+query+"&"+mlabApiKey,
       function(err, resMLab, body){
         if (err){
           var response = {
             "msg" : "Error obteniendo usuario"
           }
           res.status(500);
         }else{
           if(body.length > 0){
             var response = body[0];
           }else{
             var response= {
               "msg" : "Usuario no encontrado"
             }
             res.status(404);
           }
         }
         res.send(response);
       }
     );
}

function createUserV2(req, res){
  console.log('POST /apitechu/v2/users');
  console.log(req.body);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  var httpClient = requestJson.createClient(baseMlabURL);

  //Consulto el último usuario para obtener el id
  var query = 'f={"id" : 1,  "_id" : 0}&s={"id" : -1}&l=1';
  console.log(query);
  var ultimoId;
  var idNueva;
  //Hago la petición para obtener el último registro y conocer su id
  httpClient.get("user/?"+query+"&"+mlabApiKey,
     function(err, resMLab, body){

       if (err){
         var response = {
           "msg" : "Error obteniendo usuario"
         }

       }else{
         if(body.length > 0){
           var response = body[0];
           ultimoId = response.id;
           console.log("ultimoId" + ultimoId);

           idNueva = 1+ultimoId;
         console.log("Id del nuevo usuario: "+ idNueva);
         var newUser = {
           "id" : idNueva,
           "first_name": req.body.first_name,
           "last_name": req.body.last_name,
           "email": req.body.email,
           "password" : crypt.hash(req.body.password)
         };

         httpClient.post("user/?"+mlabApiKey, newUser,
           function(err, resMLab, body){
             console.log("Usuario guardado con éxito");
             console.log(body);
             res.status(201).send({"msg" : "usuario guardado"});

           }
         )


         }else{
           var response= {
             "msg" : "Usuario no encontrado"
           }

         }
       }
       idNueva = 1+ultimoId;
       console.log(idNueva);
       //res.send(response);
     }
   );

}

function createUserV1(req, res){
  console.log('POST /apitechu/v1/users');
  console.log(req.headers);
  console.log(req.headers.first_name);
  console.log(req.headers.last_name);
  console.log(req.headers.email);
  var io = require('../io.js');
  var newUser = {
    "first_name": req.headers.first_name,
    "last_name": req.headers.last_name,
    "email": req.headers.email
  };
  var users = require('../Usuarios.json');
  users.push(newUser);
  console.log("Otro usuario más");

  io.writeUserDataToFile(users);
  res.send(users);
}

/* Práctica Vanessa*/
function getUsersV1(req,res) {
   console.log("GET /apitechu/v1/users");
   //res.sendFile('USUARIOS.json', {root: __dirname});
   //otra forma
  var result = {};
  var users  = require('../USUARIOS.json') ;//añado un . pq estoy en otra ruta
   //res.send(users);
  var top = req.query.$top;
  var count = req.query.$count;
  if (top && count){
     console.log("recupero top ");
     var userslice = users.slice(0,top);
     console.log("contar");
     //userslice.count = users.length;
     var userscount = users.length;
     result.count=userscount;
     result.users=userslice;
     //userslice.push({"count" : userscount});
     //res.send(userslice);
     res.send(result);
  } else if (top){
     console.log("recupero top ");
     var userslice = users.slice(0,top);
     res.send(userslice);
  } else if (count){
   var userscount = users.length;
   result.count=userscount;
   result.users=users;
   res.send(result);
   //users.push({"count" : userscount});
   //res.send(users);
   }else{
  //res.send(users);
  result.users=users;
  res.send(result);
       }
}
//FIn PRactica Vanesa



function deleteUserV1(req, res){
    console.log("DELETE  /apitechu/v1/users/:id");
    console.log("Parametro del id a borrar " +  JSON.stringify(req.params.id));
      var io = require('../io.js');
    var users = require('../Usuarios.json');
    //var delete = false;
  //  users.splice(req.params.id -1, 1); //borrado por posición, no por id

  for(var i=0; i<=users.length;i++){

    if(users[i].id== req.params.id){

       users.splice(i, 1);
       console.log("Borrado  "+i);

       break;
    }
  }


    io.writeUserDataToFile(users);
    res.send(users);
}

function createUserSimpleV2(req, res){
  console.log('POST /apitechu/v2/users');
  console.log(req.body);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  var httpClient = requestJson.createClient(baseMlabURL);

   var newUser = {
     "id" : 40,
     "first_name": req.body.first_name,
     "last_name": req.body.last_name,
     "email": req.body.email,
     "password" : req.body.password
   };

   httpClient.post("user/?"+mlabApiKey, newUser,
     function(err, resMLab, body){
       console.log("Usuario guardado con éxito");
       console.log(body);
       res.status(201).send({"msg" : "usuario guardado"});

     }
   )
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.createUserSimpleV2 = createUserSimpleV2
module.exports.deleteUserV1 = deleteUserV1;
