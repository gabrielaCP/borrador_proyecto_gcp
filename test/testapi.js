const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should(); //asigna valor expected

//esto escribe por consola el resultado del test
describe('First test',
  function(){
    it('Test that duckduckGi works', function(done){
        chai.request('http://www.duckduckgo.com').get('/')  //este método llama a la raiz de la web
      //  chai.request('https://developer.mozilla.org/en-US/adsfasdfas').get('/')
        .end(
          function(err, res){
            //console.log(res);
            console.log("Request has finished");
            console.log(err);
            res.should.have.status(200);
            done(); /*este done se envía en el propio test a la función manejadora de ese test: la petición es asíncrona y
            hay que indicarle a mocha cuándo tiene que comprobar si el valor es correcto o no*/
          }
        )  //esto es un nodo terminal end con una función manejadoras
    }
    )
  }

)


//Copiamos y pegamos para hacer otro test:

describe('Test de API usuarios',
  function(){
      it('Test de que la API de usuarios funciona correctamente', function(done){
          chai.request('http://localhost:3000')
            .get('/apitechu/v1/hello')  //este método llama a la raiz de la web
            .end(
              function(err, res){
                //console.log(res);
                console.log("Request has finished");
                console.log(err);
                res.should.have.status(200);
                res.body.msg.should.be.eql("Hola estoy con un json a manini");
                done(); /*este done se envía en el propio test a la función manejadora de ese test: la petición es asíncrona y
                hay que indicarle a mocha cuándo tiene que comprobar si el valor es correcto o no*/
              }
          )  //esto es un nodo terminal end con una función manejadoras
        }
      ),
      //Añadimos otro test
      it('Test de que la API de usuarios funciona correctamente y devuelve la lista de usuarios correctos', function(done){
          chai.request('http://localhost:3000')
            .get('/apitechu/v1/users')  //este método llama a la raiz de la web  localhost:3000/apitechu/v1/users
            .end(
              function(err, res){
                //console.log(res);
                console.log("Request has finished");
                console.log(err);
                res.should.have.status(200);

                res.body.users.should.be.a('array');

                for(user of res.body.users){
                  user.should.have.property('email');
                  user.should.have.property('first_name');
                }
                //res.body.msg.should.be.eql("Hola estoy con un json a manini");
                done(); /*este done se envía en el propio test a la función manejadora de ese test: la petición es asíncrona y
                hay que indicarle a mocha cuándo tiene que comprobar si el valor es correcto o no*/
              }
          )  //esto es un nodo terminal end con una función manejadoras
      }
    )
  }

)
