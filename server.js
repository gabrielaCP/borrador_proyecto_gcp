
require('dotenv').config();

const express= require('express'); // con esta variable vamos a tener disponible este framework
const userController = require('./controllers/userControllers');

const io = require('./io');
const app= express(); // esto lo inicia

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 //OJO ,hay que incluir las cabeceras. Si utilizamos cabeceas no estandar hay que definirlas.
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}

app.use(express.json());

app.use(enableCORS);

const port = process.env.PORT || 3000; // aquí se da preferencia a la variable de entorno (por convención se ponen en MAY) y si no, se coge el puerto 3000

app.listen(port);
console.log("API Escuchando en puerto port BIP BIP: "+port);

app.get('/apitechu/v1/hello',
  function(req,res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg":"Hola estoy con un json a manini"});
  }
)

app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);
app.post('/apitechu/v1/users', userController.createUserV1);
app.post('/apitechu/v2/users', userController.createUserV2);
app.post('/apitechu/v3/users', userController.createUserSimpleV2);
app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);


const authController = require('./controllers/AuthController');

app.post('/apitechu/v1/login',authController.loginV1);
app.post('/apitechu/v1/logout/:id',authController.logoutV1);


app.post('/apitechu/v2/login',authController.loginProfeV2);
app.post('/apitechu/v2/logout/:id',authController.logoutV2);
