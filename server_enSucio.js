//console.log("Hola mundo");

const express= require('express'); // con esta variable vamos a tener disponible este framework
const userController = require('./controllers/userControllers');

const io = require('./io');
const app= express(); // esto lo inicia

app.use(express.json());

const port = process.env.PORT || 3000; // aquí se da preferencia a la variable de entorno (por convención se ponen en MAY) y si no, se coge el puerto 3000

app.listen(port);
console.log("API Escuchando en puerto port BIP BIP: "+port);
/*
app.get('/apitechu/v1/hello',
  function(req,res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg":"Hola estoy con un json a manini"});
  }
)


app.get('/apitechu/v1/users',
  function(req,res){
    console.log('GET /apitechu/v1/users');

    //Enviamos como respuesta el json de usuarios mock
    //res.sendFile('Usuarios.json', {root:__dirname});
    var users = require('./Usuarios.json'); //como usuarios devuelve un array, podemos hacer el send de esto
    var result= {};
    if(req.query.$count =="true"){
      console.log("Count needed");
      result.count = users.length;
    }
    //OPERADOR TERNARIO
    result.users = req.query.$top ? users.slice(0, req.query.$top) : users;

    res.send(result);

  }
)

*/

app.get('/apitechu/v1/users', userController.getUsersV1);
app.post('/apitechu/v1/users', userController.createUserV1);
app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);

/*app.post('/apitechu/v1/users',
  function(req, res){
      console.log('POST /apitechu/v1/users');
      console.log(req.headers);
      console.log(req.headers.first_name);
      console.log(req.headers.last_name);
      console.log(req.headers.email);

      var newUser = {
        "first_name": req.headers.first_name,
      "last_name": req.headers.last_name,
      "email": req.headers.email
      };
      var users = require('./Usuarios.json');
      users.push(newUser);
      console.log("Otro usuario más");

      writeUserDataToFile(users);
      res.send(users);
})

*/
app.put('/apitechu/v1/users',
  function(req, res){
      console.log('POST /apitechu/v1/users');
      console.log(req.headers);
      console.log(req.headers.first_name);
      console.log(req.headers.last_name);
      console.log(req.headers.email);

      var newUser = {
        "first_name": req.headers.first_name,
      "last_name": req.headers.last_name,
      "email": req.headers.email
      };
      var users = require('./Usuarios.json');
      users.push(newUser);
      console.log("Otro usuario más");

      const fs = require('fs');

      var jsonUserData = JSON.stringify(users);

      fs.writeFile("./Usuarios.json", jsonUserData, "utf8",
        function(err){
          if (err){
            console.log(err);
          }else{
            console.log("OK");
          }
        }

      )
      res.send({"id": "nuevo ID"});
})
/*
function writeUserDataToFile(users){
    console.log("writeUserDataToFile");
    const fs = require('fs');
    var jsonUserData = JSON.stringify(users);

    fs.writeFile("./Usuarios.json", jsonUserData, "utf8",
      function(err){
        if (err){
          console.log(err);
        }else{
          console.log("OK de la funcion");
        }
      }

    )
}
*/
/*
app.post('/apitechu/v1/monstruo/:p1/:p2',
  function (req, res){
    console.log("Parametros " +  JSON.stringify(req.params));
    console.log("Query " +  JSON.stringify(req.query));
    console.log("Headers " +  JSON.stringify(req.headers));
    console.log("Body " +  JSON.stringify(req.body));


    console.log(req.body);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);

    var newUser = {
      "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
    };
    var users = require('./Usuarios.json');
    users.push(newUser);
    console.log("Otro usuario más");

    writeUserDataToFile(users);
    res.send(users);
  }
)*/
/*
  app.delete('/apitechu/v1/users/:id',
    function (req, res){
        console.log("DELETE  /apitechu/v1/users/:id");
        console.log("Parametro del id a borrar " +  JSON.stringify(req.params.id));

        var users = require('./Usuarios.json');
        //var delete = false;
      //  users.splice(req.params.id -1, 1); //borrado por posición, no por id

      //  deleteItem1(users, req.params.id);
        deleteItem2(users, req.params.id);
      //  deleteItem3(users, req.params.id);


        writeUserDataToFile(users);
        res.send(users);
    }
)
*/


/*
function deleteItem1(lista, position){
    console.log("deleteItem1");
    console.log("longitud de la lista "+lista.length);

    for(var i=0; i<=lista.length;i++){

      console.log(lista[i].id);

      if(lista[i].id== position){
        console.log("Id encontrado "+ lista[i].id);
         lista.splice(i, 1);
         console.log("Borrado  "+i);

         break;
      }
    }
  }
*/
function deleteItem2(lista, position){
    console.log("deleteItem2");
    console.log("longitud de la lista 2 :"+lista.length);
    var count = lista.length;
        if (count > 0) {
            console.log("longitud de la lista 2 con count "+count);
            lista.forEach(function(lista,position){
              var indexOfElement = lista.findIndex(
                  function(element){
                    console.log("comparando: "+element.id + " y "+position);
                    return element.id == position;
                  });
                  if (indexOfElement >= 0){
                    lista.splice(indexOfElement, 1);
                    console.log("el for each funciona "+indexOfElement);
                  }
                /*  if(lista.id == position){
                    console.log("el for each funciona "+position);
                    users.splice (position,1);
                  }*/


/*

                if(lista.id == position){
                  console.log("coincide " + position);
                  lista.splice(i, 1);
                  console.log("Borrado  "+i);
                  //En el for each no se puede hacer break
                }*/
            });
          }
 }

 function deleteItem3(lista, position){
     console.log("deleteItem3");
     console.log("longitud de la lista 3 :"+lista.length);
     var count = lista.length;
     for (var i in lista) {
      // do something
      if(lista[i].id== position){
        console.log("Id encontrado "+ lista[i].id);
         lista.splice(i, 1);
         console.log("Borrado  "+i);
         break;
    }
  }
}

function deleteItem4(lista, position){
    console.log("deleteItem3");
    console.log("longitud de la lista 3 :"+lista.length);
    var count = lista.length;
    for (var i in lista) {
     // do something
     if(lista[i].id== position){
       console.log("Id encontrado "+ lista[i].id);
        lista.splice(i, 1);
        console.log("Borrado  "+i);
        break;
   }
 }
}


function deleteItemProfe(lista, position){
  app.delete('/apitechu/v1/users/:id',
function(req, res) {
 console.log("DELETE /apitechu/v1/users/:id");
 console.log("id es " + req.params.id);

 var users = require('./usuarios.json');
 var deleted = false;

 // console.log("Usando for normal");
 // for (var i = 0; i < users.length; i++) {
 //   console.log("comparando " + users[i].id + " y " +  req.params.id);
 //   if (users[i].id == req.params.id) {
 //     console.log("La posicion " + i + " coincide");
 //     users.splice(i, 1);
 //     deleted = true;
 //     break;
 //   }
 // }

 // console.log("Usando for in");
 // for (arrayId in users) {
 //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
 //   if (users[arrayId].id == req.params.id) {
 //     console.log("La posicion " + arrayId " coincide");
 //     users.splice(arrayId, 1);
 //     deleted = true;
 //     break;
 //   }
 // }

 // console.log("Usando for of");
 // for (user of users) {
 //   console.log("comparando " + user.id + " y " +  req.params.id);
 //   if (user.id == req.params.id) {
 //     console.log("La posición ? coincide");
 //     // Which one to delete? order is not guaranteed...
 //     deleted = false;
 //     break;
 //   }
 // }

 // console.log("Usando for of 2");
 // // Destructuring, nodeJS v6+
 // for (var [index, user] of users.entries()) {
 //   console.log("comparando " + user.id + " y " +  req.params.id);
 //   if (user.id == req.params.id) {
 //     console.log("La posicion " + index + " coincide");
 //     users.splice(index, 1);
 //     deleted = true;
 //     break;
 //   }
 // }

 // console.log("Usando for of 3");
 // var index = 0;
 // for (user of users) {
 //   console.log("comparando " + user.id + " y " +  req.params.id);
 //   if (user.id == req.params.id) {
 //     console.log("La posición " + index + " coincide");
 //     users.splice(index, 1);
 //     deleted = true;
 //     break;
 //   }
 //   index++;
 // }

 // console.log("Usando Array ForEach");
 // users.forEach(function (user, index) {
 //   console.log("comparando " + user.id + " y " +  req.params.id);
 //   if (user.id == req.params.id) {
 //     console.log("La posicion " + index + " coincide");
 //     users.splice(index, 1);
 //     deleted = true;
 //   }
 // });

 // console.log("Usando Array findIndex");
 // var indexOfElement = users.findIndex(
 //   function(element){
 //     console.log("comparando " + element.id + " y " +   req.params.id);
 //     return element.id == req.params.id
 //   }
 // )
 //
 // console.log("indexOfElement es " + indexOfElement);
 // if (indexOfElement >= 0) {
 //   users.splice(indexOfElement, 1);
 //   deleted = true;
 // }

 if (deleted) {
   // io.writeUserDataToFile(users);
  // writeUserDataToFile(users);
 }

 var msg = deleted ?
 "Usuario borrado" : "Usuario no encontrado."

 console.log(msg);
 res.send({"msg" : msg});
}
)
}


const authController = require('./controllers/AuthController');

app.post('/apitechu/v1/login',authController.loginV1);
app.post('/apitechu/v1/logout/:id',authController.logoutV1);
