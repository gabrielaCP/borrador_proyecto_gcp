const bcrypt = require('bcrypt');

function hash(data){
    console.log("Hashing data");
    return bcrypt.hashSync(data, 10); //esta no tiene funciñon manejadora porque es síncrona


}
//Función para el login
function checkpassword(passwdordPlainText, passwdordFromDBHashed){
    console.log("Checking password");
    return bcrypt.compareSync(passwdordPlainText, passwdordFromDBHashed);
}


module.exports.hash = hash;
