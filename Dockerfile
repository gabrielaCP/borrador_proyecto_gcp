#Imagen raiz
FROM node

#Carpeta raiz
WORKDIR /apitechu

#Copia de archivos de carpeta (de todo lo que hay de nuestro código fuente) local a la imagen en la carpeta apitechu
ADD . /apitechu

#tras la copia, hay que instalar las dependencias del package
RUN npm install

#Puerto que expone:
EXPOSE 3000

#Comando de inicialización: lo que hay en CMD se ejecuta con un RUN
CMD ["npm", "start"]

#Vamos a hacer que se ignore la carpeta .git y la carpeta node_modules. También habría que evitar subir las apiKeys: creamos un .dockerignore
